# Prepare

## Create *.env*:
```dotenv
# Airflow
AIRFLOW_BASE_IMAGE=home_credit_base_airflow_docker_local
AIRFLOW_RUN_DIR=/tmp/airflow

# Airflow database
AIRFLOW_DB_NAME=airflow
AIRFLOW_DB_USER=airflow
AIRFLOW_DB_PASSWORD=airflow

# Storages
HOME_CREDIT_STORAGE=</absolute/path/to/home_credit_default/storage>
HOME_CREDIT_DVC_STORAGE=</absolute/path/to/home_credit_default/dvc/remote/storage>
```

## Create supporting folders for running DAGs

- AIRFLOW_RUN_DIR to keep code, temporary data and monitoring database directory

```bash
export $(grep -v '#.*' .env | xargs)
mkdir -p ${AIRFLOW_RUN_DIR}
```


## Run Airflow cluster

```bash
docker-compose up -d
```

## Stop Airflow cluster

```bash
docker-compose down
```

## Run DAGs

go to [airflow UI](http://localhost:8080).

- *login*: airflow
- *password*: airflow
